/*
 * Application
 *
 * v.0
 *
 * Орляхина Е.
 */

import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotik1 = new Kotik();
        kotik1.setKotik(8, "Василий", 3, "Мяв", "молоко");
        Kotik kotik2 = new Kotik(10, "Тимофей", 2, "Мау", "мясо");
        System.out.println("Режим дня кота по имени " + kotik2.getName() + " весом " + kotik2.getWeight() + " кг:");
        kotik2.liveAnotherDay();
        if (kotik2.getMeow().equals(kotik1.getMeow())) {
            System.out.println("Коты " + kotik1.getName() + " и " + kotik2.getName() + " разговаривают одинаково");
        } else {
            System.out.println("Коты " + kotik1.getName() + " и " + kotik2.getName() + " разговаривают по-разному");
        }
        System.out.println("Количество котиков, созданных в процессе выполнения программы: " + Kotik.getCount());
    }
}
