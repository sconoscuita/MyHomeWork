/*
 * Kotik
 *
 * v.0
 *
 * Орляхина Е.
 */
package model;

public class Kotik {
    private static int count;
    private String name;
    private String dish;
    private String meow;
    private int weight;
    private int prettiness; //под переменной prettiness принято количество условных единиц сытости кота

    public Kotik(int prettiness, String name, int weight, String meow, String dish) {
        Kotik.count++;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.prettiness = prettiness;
        this.dish = dish;
    }

    public Kotik() {
        Kotik.count++;
    }

    public void setKotik(int prettiness, String name, int weight, String meow, String dish) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.dish = dish;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            int x = (int) (5 * Math.random() + 1);
            switch (x) {
                case 1:
                    if (this.play()) {
                        System.out.println((i+1) + ". Поиграл");
                    } else {
                        askToEat(i);
                    }
                    break;

                case 2:
                    if (this.sleep()) {
                        System.out.println((i+1) + ". Поспал");
                    } else {
                        askToEat(i);
                    }
                    break;

                case 3:
                    if (this.chase()) {
                        System.out.println((i+1) + ". Поймал мышь");
                    } else {
                        askToEat(i);
                    }
                    break;

                case 4:
                    if (this.walk()) {
                        System.out.println((i+1) + ". Погулял");
                    } else {
                        askToEat(i);
                    }
                    break;

                case 5:
                    if (this.mrMrMr()) {
                        System.out.println((i+1) + ". Помурлыкал");
                    } else {
                        askToEat(i);
                    }
                    break;
            }
        }
    }

    public void askToEat(int i) {
        eat();
        eat(2);
        eat(3, "мясо");
        System.out.println((i+1) + ". Cказал \"" + this.meow + "!\", cъел " + this.dish + " и готов действовать");
    }

    public void eat(int prettiness) {
        this.prettiness += prettiness;

    }

    public void eat(int prettiness, String dish) {
        this.prettiness += prettiness;
        this.dish = dish;
    }

    public void eat() {
        eat(1, "мясо");
    }

    public boolean play() {
        if (prettiness > 0) {
            this.prettiness--;
            return true;
        } else {
            return false;
        }
    }

    public boolean sleep() {
        if (prettiness > 0) {
            this.prettiness--;
            return true;
        } else {
            return false;
        }
    }

    public boolean chase() {
        if (prettiness > 0) {
            this.prettiness--;
            return true;
        } else {
            return false;
        }
    }

    public boolean walk() {
        if (prettiness > 0) {
            this.prettiness--;
            return true;
        } else {
            return false;
        }
    }

    public boolean mrMrMr() {
        if (prettiness > 0) {
            this.prettiness--;
            return true;
        } else {
            return false;
        }
    }

    public static int getCount() {
        return count;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.name = meow;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }
}
