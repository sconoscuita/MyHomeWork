
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class AvitoPage {

    public WebDriver driver;
    public Select select;
    static WebDriverWait wait;

    public AvitoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div/select[contains(@data-marker, 'search-form/category')]")
    public WebElement selectCategory;

    @FindBy(xpath = "//div//input[contains(@data-marker, 'search-form/suggest')]")
    public WebElement inputForm;


    @FindBy(xpath = "//div[contains(@data-marker, 'search-form/region')]")
    public WebElement searchRegionField;

    @FindBy(xpath = "//div/input[contains(@data-marker, 'popup-location/region/input')]")
    public WebElement inputRegion;

    @FindBy(xpath = "//div/ul/li[contains(@data-marker, 'suggest(0)')]")
    public WebElement selectFirstRegion;

    @FindBy(xpath = "//div/button[contains(@data-marker, 'popup-location/save-button')]")
    public WebElement searchButton;

    @FindBy(xpath = "//label[./input[contains(@name, 'withImagesOnly')]]")
    public WebElement checkBoxWithImagesOnly;

    @FindBy(xpath = "//div/label/span[contains(@data-marker, 'delivery-filter/text')]")
    public WebElement checkBoxDelivery;

    @FindBy(xpath = "//div/button[contains(@data-marker, 'search-filters/submit-button')]")
    public WebElement searchByFilterButton;

    @FindBy(xpath = "//div/select/option[contains(text(), 'Дороже')]/..")
    public WebElement selectPriceFilter;

    @FindBy(xpath = "//div[@data-marker='catalog-serp']/div[@data-marker='item'][position() <= 10]")
    public List<WebElement> printBySort;

    @FindBy(xpath = "//div/select[contains(@data-marker, 'search-form/category')]/option[@value >= 1]")
    public List<WebElement> category;

    void selectCategory(String category) {
        select = new Select(selectCategory);
        select.selectByVisibleText(category);
    }

    void sendProductToInputForm(String product) {
        inputForm.sendKeys(product);
    }

    void searchRegionField() {
        wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(searchRegionField));
        searchRegionField.click();
    }

    void inputRegion(String region) {
        wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(inputRegion));
        inputRegion.sendKeys(region);
    }

    void selectFirstRegion(String region) {
        wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.textToBePresentInElement(selectFirstRegion, region));
        selectFirstRegion.click();
    }

    void searchButton() {
        searchButton.click();
    }

    void searchByFilterButton() {
        searchByFilterButton.click();
    }

    void selectByPriceSort(String condition) {
        select = new Select(selectPriceFilter);
        select.selectByVisibleText(condition);
    }

    void selectCheckBoxWithImagesOnly() {
        wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(checkBoxWithImagesOnly));
        if (!checkBoxWithImagesOnly.isSelected()) {
            checkBoxWithImagesOnly.click();
        }
    }

    void selectCheckBoxDelivery() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", checkBoxDelivery);
        wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(checkBoxDelivery));
        if (!checkBoxDelivery.isSelected()) {
            checkBoxDelivery.click();
        }
    }

    void printBySort(int count) {
        for (int i = 0; i < count; i++) {
            System.out.println("Принтер : " +
                    printBySort.get(i).findElement(By.xpath(".//h3[@itemprop = 'name']")).getText() + "\n" +
                    "Стоимость : " +
                    printBySort.get(i).findElement(By.xpath(".//span[@data-marker = 'item-price']")).getText() + "\n"
            );
        }
    }

//    void categoryGenerator() {
//
//        for (int i = 0; i < category.size(); i++) {
//
//            String index = category.get(i).getAttribute("value");
//            String text = category.get(i).getText();
//
//            System.out.println(text);
//        }
//    }
}