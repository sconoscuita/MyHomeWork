
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class UItests {
    static WebDriver driver;
    static AvitoPage page;
    static String category;
    static String product;
    static String region;
    static String condition;
    static int count;

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        page = new AvitoPage(driver);
        category = "Оргтехника и расходники";
        product = "Принтер";
        region = "Владивосток";
        condition = "Дороже";
        count = 3;

        driver.get("https://www.avito.ru/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        page.selectCategory(category);
        page.sendProductToInputForm(product);
        page.searchRegionField();
        page.inputRegion(region);
        page.selectFirstRegion(region);
        page.searchButton();
        page.selectCheckBoxWithImagesOnly();
        page.selectCheckBoxDelivery();
        page.searchByFilterButton();
        page.selectByPriceSort(condition);
        page.printBySort(count);
    }
}