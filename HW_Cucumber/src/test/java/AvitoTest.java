import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/java/features",
        glue = "stepDef",
        tags = "@1"
)
public class AvitoTest extends AbstractTestNGCucumberTests {
}
