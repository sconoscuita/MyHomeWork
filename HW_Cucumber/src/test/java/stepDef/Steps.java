package stepDef;

import io.cucumber.java.ru.Тогда;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import org.testng.Assert;
import java.util.concurrent.TimeUnit;

public class Steps {
    @ParameterType(".*")
    public CategoryEnum category(String category) {
        return CategoryEnum.valueOf(category);
    }

    @ParameterType(".*")
    public SortByConditionEnum sort(String condition) {
        return SortByConditionEnum.valueOf(condition);
    }

    @Пусть("открыт ресурс авито")
    public void getAvito() {
        Hook.driver.get("https://www.avito.ru/");
        Hook.driver.manage().window().maximize();
        Hook.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @И("в выпадающем списке категорий выбрана {category}")
    public void selectCategory(CategoryEnum category) {
        Hook.page.selectCategory(category.getText());
    }

    @И("в поле поиска введено значение {string}")
    public void product(String product) {
        Hook.page.sendProductToInputForm(product);
    }

    @Тогда("кликнуть по выпадающему списку региона")
    public void searchRegion() {
        Hook.page.searchRegionField();
    }

    @Тогда("в поле регион введено значение {string}")
    public void inputRegion(String region) {
        Hook.page.inputRegion(region);
        Hook.page.selectFirstRegion(region);
    }

    @И("нажата кнопка показать объявления")
    public void searchButton() {
        Hook.page.searchButton();
    }

    @Тогда("открылась страница результаты по запросу {string}")
    public void checkingByProduct(String product) {
        Assert.assertTrue(Hook.page.checkingByProduct().contains(product));
    }

    @И("активирован чекбокс только с фото")
    public void selectCheckBoxWithImagesOnly() {
        Hook.page.selectCheckBoxWithImagesOnly();
    }

    @И("в выпадающем списке сортировка выбрано значение {sort}")
    public void searchByFilter(SortByConditionEnum condition) {
        Hook.page.searchByFilterButton();
        Hook.page.selectByPriceSort(condition.getText());
    }

    @И("в консоль выведено значение названия и цены {int} первых объявлений")
    public void printBySort(int count) {
        Hook.page.printBySort(count);
    }

}
