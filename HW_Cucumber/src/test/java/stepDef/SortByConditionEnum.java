package stepDef;

public enum SortByConditionEnum {
    ПО_УМОЛЧАНИЮ(101, "По умолчанию"),
    ДЕШЕВЛЕ(1, "Дешевле"),
    ДОРОЖЕ(2, "Дороже"),
    ПО_ДАТЕ(104,"По дате");

    private String text;
    private int index;

    SortByConditionEnum(int index, String text) {
        this.index = index;
        this.text = text;
    }

    public String getText() {
        return text;
    }
    public int getIndex() {
        return index;
    }
}
