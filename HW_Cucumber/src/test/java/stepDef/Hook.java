package stepDef;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObject.AvitoPage;


public class Hook {
    static WebDriver driver;
    static AvitoPage page;

    @Before
    public void open() {
        System.setProperty("webdriver.chrome.driver", "C:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        page = new AvitoPage(driver);
    }

    @After
    public void close() {
        driver.quit();
    }
}
