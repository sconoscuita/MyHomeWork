#language: ru
@1
Функционал: Поиск на авито

  Структура сценария: Найдем '<товар>' в '<регион>' на авито
    Пусть открыт ресурс авито
    И в выпадающем списке категорий выбрана <категория>
    И в поле поиска введено значение '<товар>'
    Тогда кликнуть по выпадающему списку региона
    Тогда в поле регион введено значение '<регион>'
    И нажата кнопка показать объявления
    Тогда открылась страница результаты по запросу '<товар>'
    И активирован чекбокс только с фото
    И в выпадающем списке сортировка выбрано значение <условие>
    И в консоль выведено значение названия и цены <число> первых объявлений

  Примеры:
  | категория               | товар       | регион      | условие    | число  |
  | ОРГТЕХНИКА_И_РАСХОДНИКИ | принтер     | Владивосток | ДОРОЖЕ     | 3      |
  | АВТОМОБИЛИ              | ваз 2106    | Саратов     | ДЕШЕВЛЕ    | 5      |
  | НЕДВИЖИМОСТЬ            | квартира    | Москва      | ДОРОЖЕ     | 3      |
  | ЖИВОТНЫЕ                | хаски       | Барнаул     | ПО_ДАТЕ    | 5      |
