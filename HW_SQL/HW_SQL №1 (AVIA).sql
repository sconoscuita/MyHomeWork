--№1.	Вывести список самолетов с кодами 320, 321, 733
SELECT *
FROM LANIT.AIRCRAFTS_DATA ad 
WHERE (ad.AIRCRAFT_CODE = '320') OR (ad.AIRCRAFT_CODE = '321') OR (ad.AIRCRAFT_CODE = '733')

--№2.	Вывести список самолетов с кодом не на 3
SELECT *
FROM LANIT.AIRCRAFTS_DATA ad 
WHERE ad.AIRCRAFT_CODE NOT LIKE '3%'

--№3.	Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла
SELECT *
FROM LANIT.TICKETS t
WHERE UPPER(t.PASSENGER_NAME) LIKE 'OLGA%' AND ((UPPER(t.EMAIL) LIKE 'OLGA%') OR (t.EMAIL IS NULL))

--№4.	Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета
SELECT *
FROM LANIT.AIRCRAFTS_DATA ad
WHERE (ad."RANGE" = 5600) OR (ad."RANGE" = 5700)
ORDER BY ad."RANGE" DESC

--№5.	Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию
SELECT ad.AIRPORT_NAME, ad.CITY
FROM LANIT.AIRPORTS_DATA ad
WHERE ad.CITY = 'Moscow'
ORDER BY ad.AIRPORT_NAME

--№6.	Вывести список всех городов без повторов в зоне «Europe»
SELECT DISTINCT ad.CITY
FROM LANIT.AIRPORTS_DATA ad
WHERE ad.TIMEZONE LIKE 'Europe%'
ORDER BY ad.CITY

--№7.	Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT b.BOOK_REF, b.TOTAL_AMOUNT*0.9
FROM LANIT.BOOKINGS b
WHERE b.BOOK_REF LIKE '3A4%'

--№8.	Вывести все данные по местам в самолете с кодом 320 и классом «Business»в формате «Данные по месту: номер места»
SELECT 'Данные по месту: '|| s.SEAT_NO
FROM LANIT.SEATS s
WHERE s.AIRCRAFT_CODE = '320' AND s.FARE_CONDITIONS = 'Business'

--№9.	Найти максимальную и минимальную сумму бронирования в 2017 году
SELECT MAX(b.TOTAL_AMOUNT), MIN(b.TOTAL_AMOUNT) 
FROM LANIT.BOOKINGS b
WHERE TRUNC(b.BOOK_DATE, 'YEAR') = TO_DATE('01.01.2017', 'dd.mm.yyyy')

--№10.	Найти количество мест во всех самолетах
SELECT s.AIRCRAFT_CODE, COUNT(*) 
FROM LANIT.SEATS s
GROUP BY s.AIRCRAFT_CODE

--№11.	Найти количество мест во всех самолетах с учетом типа места
SELECT s.AIRCRAFT_CODE, s.FARE_CONDITIONS, COUNT(*) 
FROM LANIT.SEATS s
GROUP BY s.AIRCRAFT_CODE, s.FARE_CONDITIONS
ORDER BY 1,2

--№12.	Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11
SELECT COUNT(*)
FROM TICKETS t
WHERE UPPER(t.PASSENGER_NAME)='ALEKSANDR STEPANOV' AND t.PHONE LIKE '%11'

--№13.	Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000.
--Отсортировать по убыванию количества билетов
SELECT t.PASSENGER_NAME, COUNT(*) AS TICKETS_COUNT
FROM TICKETS t
WHERE UPPER(t.PASSENGER_NAME) LIKE 'ALEKSANDR%'
GROUP BY t.PASSENGER_NAME
HAVING COUNT(*)>2000
ORDER BY TICKETS_COUNT DESC

--№14.	Вывести дни в сентябре 2017 с количеством рейсов больше 500
SELECT TRUNC(f.DATE_DEPARTURE, 'DDD'), COUNT(*) AS COUNT_FLIGHTS
FROM LANIT.FLIGHTS f
WHERE TRUNC(f.DATE_DEPARTURE, 'DDD') BETWEEN TO_DATE('01.09.2017', 'dd.mm.yyyy') AND TO_DATE('30.09.2017', 'dd.mm.yyyy')
GROUP BY TRUNC(f.DATE_DEPARTURE, 'DDD')
HAVING COUNT(*)>500

--№15.	Вывести список городов, в которых несколько аэропортов
SELECT ad.CITY, COUNT(*) 
FROM LANIT.AIRPORTS_DATA ad
GROUP BY ad.CITY
HAVING COUNT(*)>1

--№16.	Вывести модель самолета и список мест в нем
SELECT ad.MODEL, s.SEAT_NO
FROM LANIT.AIRCRAFTS_DATA ad JOIN LANIT.SEATS s ON ad.AIRCRAFT_CODE=s.AIRCRAFT_CODE

--№17.	Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017
SELECT * 
FROM LANIT.AIRPORTS_DATA ad JOIN LANIT.FLIGHTS f ON ad.AIRPORT_CODE=f.DEPARTURE_AIRPORT
WHERE TRUNC(f.DATE_DEPARTURE, 'MONTH') = TO_DATE('01.09.2017', 'dd.mm.yyyy') AND ad.CITY='Moscow'

--№18.	Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017
SELECT ad.AIRPORT_CODE, COUNT(*) 
FROM LANIT.AIRPORTS_DATA ad JOIN LANIT.FLIGHTS f ON ad.AIRPORT_CODE=f.DEPARTURE_AIRPORT
WHERE EXTRACT (YEAR FROM f.DATE_DEPARTURE)=2017 AND ad.CITY='Moscow'
GROUP BY ad.AIRPORT_CODE

--№19.	Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017
SELECT ad.AIRPORT_CODE, EXTRACT (MONTH FROM f.DATE_DEPARTURE) AS "MONTH", COUNT(*) AS "COUNT_FLIGHTS"
FROM LANIT.AIRPORTS_DATA ad JOIN LANIT.FLIGHTS f ON ad.AIRPORT_CODE=f.DEPARTURE_AIRPORT
WHERE EXTRACT (YEAR FROM f.DATE_DEPARTURE)=2017 AND ad.CITY='Moscow'
GROUP BY ad.AIRPORT_CODE, EXTRACT (MONTH FROM f.DATE_DEPARTURE)
ORDER BY 1,2

--№20.	Найти все билеты по бронированию на «3A4B»
SELECT *
FROM LANIT.TICKETS t
WHERE t.BOOK_REF LIKE '3A4B%'

--№21.	Найти все перелеты по бронированию на «3A4B»
SELECT tf.FLIGHT_ID
FROM LANIT.TICKETS t JOIN LANIT.TICKET_FLIGHTS tf ON t.TICKET_NO = tf.TICKET_NO
WHERE t.BOOK_REF LIKE '3A4B%'







