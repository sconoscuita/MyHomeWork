--№1. Найдите номер модели, скорость и размер жесткого диска для всех ПК стоимостью менее 500 дол. Вывести: model, speed и hd
SELECT model, speed, hd
FROM PC
WHERE price < 500
ORDER BY speed DESC

--№2. Найдите производителей принтеров. Вывести: maker
SELECT DISTINCT maker
FROM Product
WHERE type = 'Printer'

--№3. Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.
SELECT model, ram, screen
FROM Laptop
WHERE price > 1000
ORDER BY price ASC

--№4. Найдите все записи таблицы Printer для цветных принтеров.
SELECT *
FROM printer
WHERE color = 'y'

--№5. Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол.
SELECT model, speed, hd
FROM PC
WHERE ((cd = '12x') OR (cd = '24x')) AND (price < 600)

--№6. Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти скорости таких ПК-блокнотов. Вывод: производитель, скорость.
SELECT DISTINCT p.maker, l.speed
FROM Laptop l JOIN Product p ON p.model=l.model
WHERE l.hd >= 10

--№7. Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).
SELECT Product.model, PC.price
FROM Product JOIN PC ON Product.model=PC.model
WHERE maker='B'
UNION
SELECT Product.model, Laptop.price
FROM Product JOIN Laptop ON Product.model=Laptop.model
WHERE maker='B'
UNION
SELECT Product.model, Printer.price
FROM Product JOIN Printer ON Product.model=Printer.model
WHERE maker='B'

--№8. Найдите производителя, выпускающего ПК, но не ПК-блокноты.
SELECT maker FROM Product
WHERE type='PC'
EXCEPT (
SELECT maker FROM Product
WHERE type='PC'
INTERSECT
SELECT maker FROM Product
WHERE type='Laptop')

--№9. Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker
SELECT DISTINCT maker
FROM Product JOIN PC ON Product.model=PC.model
WHERE speed>=450

--№10. Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price
SELECT model, price FROM Printer
WHERE price = (SELECT MAX(price) FROM Printer)

--№11. Найдите среднюю скорость ПК.
SELECT AVG(speed) FROM PC

--№12. Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.
SELECT AVG(speed) AS AVG_speed
FROM (SELECT speed FROM Laptop
WHERE price > 1000) x

--№13. Найдите среднюю скорость ПК, выпущенных производителем A.
SELECT AVG(speed) AS AVG_speed
FROM (SELECT speed FROM Product JOIN PC ON Product.model=PC.model
WHERE maker='A') x

--№14. Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.
SELECT Classes.class, name, country
FROM Classes JOIN Ships ON Classes.class=Ships.class
WHERE numGuns >= 10

--№15. Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
SELECT hd FROM PC
GROUP BY hd
HAVING count(hd)>=2

--№16. Найдите пары моделей PC, имеющих одинаковые скорость и RAM. 
--В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i).
--Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM.
SELECT DISTINCT A.model AS model_1, B.model AS model_2, A.speed, A.ram
FROM PC AS A, PC B
WHERE A.speed=B.speed AND A.ram=B.ram AND A.model > B.model


--№17. Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК. Вывести: type, model, speed
SELECT DISTINCT type, Product.model, speed
FROM Laptop, Product
WHERE Product.model=Laptop.model AND 
	Laptop.speed < ALL(SELECT PC.speed FROM PC)


--№18. Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price
--Вывести: maker, средний размер экрана.
SELECT DISTINCT maker, price
FROM Printer, Product
WHERE Product.model=Printer.model AND 
	Printer.color='y' AND 
	Printer.price =(SELECT MIN(price) FROM Printer WHERE color='y')

--№19. Для каждого производителя, имеющего модели в таблице Laptop, найдите средний размер экрана выпускаемых им ПК-блокнотов.
--Вывести: maker, средний размер экрана.
SELECT maker, AVG(screen) AS AVG_screen
FROM Product JOIN Laptop ON Product.model=Laptop.model
GROUP BY maker

--№20. Найдите производителей, выпускающих по меньшей мере три различных модели ПК. Вывести: Maker, число моделей ПК.
SELECT maker, count(model)
FROM Product
WHERE type='PC'
GROUP BY maker
HAVING count(model)>=3

--№21. Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
--Вывести: maker, максимальная цена.
SELECT maker, MAX(price)
FROM Product JOIN PC ON Product.model=PC.model
GROUP BY maker






