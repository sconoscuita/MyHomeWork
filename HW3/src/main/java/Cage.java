import animals.Animal;

import java.util.HashMap;

public class Cage<T extends Animal> {
    private HashMap<String, T> cell;
    private CageSize size;
    private int area;
    private String name;

    public Cage(CageSize size, String name) {
        this.cell = new HashMap<>();
        this.size = size;
        this.area = size.getSqMeter();
        this.name = name;
    }

    public void add(T... animals) {
        for (int i = 0; i < animals.length; i++) {
            if (area < animals[i].getArea()) {
                System.out.println(animals[i].getName() + " не помещается в вольер "  + this.name
                        + ". В вольере осталось " + area + " кв.м. свободного места, а " + animals[i].getName()
                        + " требует " + animals[i].getArea() + " кв.м.");
                continue;
            }
            if (cell.containsKey(animals[i].getName())) {
                System.out.println("Животное по имени " + animals[i].getName() + " уже есть в вольере " + this.name);
                continue;
            }
            cell.put(animals[i].getName(), animals[i]);
            area -= animals[i].getArea();
            System.out.println("Животное по имени " + animals[i].getName() + " помещено в вольер "
                    + this.name + ". В вольере осталось " + area + " кв.м. свободного места");
        }
    }

    public void delete(Animal animal) {
        cell.remove(animal.getName(), animal);
        this.area += animal.getArea();
        System.out.println("Животное по имени " + animal.getName() + " извлечено из вольера. В вольере "
                + this.name + " осталось " + area + " кв.м. свободного места");
    }

    public Animal get(String name) {
        if (!cell.containsKey(name)) {
            System.out.println("Животное по имени " + name + " отсутствует вольере "
                    + this.name + ", невозможно получить ссылку");
            return null;
        }
        System.out.println("Переменной присвоена ссылка на объект животного по имени " + name);
        return cell.get(name);
    }

    public CageSize getSize() {
        return size;
    }

    public void setSize(CageSize size) {
        this.size = size;
    }
}
