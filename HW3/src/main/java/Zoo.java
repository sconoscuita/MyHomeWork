import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

import java.util.HashMap;
import java.util.Map;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker();

        Grass grass = new Grass(100, "зелень");
        Meat meat = new Meat(200, "мясо");

        Lion lion = new Lion(50, "Лев");
        Beaver beaver = new Beaver(50, "Бобер");
        Eagle eagle = new Eagle(50, "Орел");
        Eagle eagle2 = new Eagle(50, "Орел");
        Fish fish = new Fish(50, "Рыба");
        Fish fish1 = new Fish(50, "Рыба1");
        Fish fish2 = new Fish(50, "Рыба2");
        Fish fish3 = new Fish(50, "Рыба3");
        Fish fish4 = new Fish(50, "Рыба4");
        Duck duck = new Duck(50, "Утка");
        Elephant elephant = new Elephant(50, "Слон");

        System.out.println("\n" + "Посмотрим на пруд:" + "\n");
        Swim[] lake = {beaver, duck, fish};
        for (int i = 0; i < lake.length; i++) {
            lake[i].swim();
        }

        System.out.println("\n" + "Попросим животных подать голос:" + "\n");
        worker.getVoice(duck);
        worker.getVoice(eagle);
        worker.getVoice(lion);

        System.out.println("\n" + "Поиграем с уткой:" + "\n");
        duck.fly();
        duck.run();
        duck.swim();
        worker.getVoice(duck);

        System.out.println("\n" + "Покормим животных:" + "\n");
        HashMap<Animal, Food> collection = new HashMap<>();
        collection.put(lion, grass);
        collection.put(eagle, meat);
        collection.put(beaver, meat);
        collection.put(elephant, grass);
        collection.put(duck, grass);
        for (Map.Entry<Animal, Food> pair : collection.entrySet() ) {
            try {
                worker.feed(pair.getKey(), pair.getValue());
            }
            catch (Exception wrongFoodException) {
                System.out.println(pair.getKey().getName() + " не ест " + pair.getValue().getType());
            }
        }

        //создаем вольеры
        Cage<Carnivorous> carnivorousCage = new Cage<>(CageSize.LARGE, "\"Хищники\"");
        Cage<Herbivore> herbivoreCage = new Cage<>(CageSize.EXTRA_LARGE, "\"Травоядные\"");
        Cage<Lion> lionCage = new Cage<>(CageSize.MEDIUM, "\"Львы\"");
        Cage<Fish> fishCage = new Cage<>(CageSize.SMALL, "\"Рыбки\"");

        System.out.println("\nЗаполним вольер для травоядных:");
        herbivoreCage.add(elephant, beaver, duck);
        herbivoreCage.add(elephant);

        System.out.println("\nЗаполним вольер для хищников:");
        carnivorousCage.add(lion, eagle, fish); //бедная рыбка в сухом вольере, ну пусть будет
        carnivorousCage.add(eagle2);

        System.out.println("\nЗаполним вольер для рыб:");
        fishCage.add(fish, fish1, fish2, fish3, fish4);

        System.out.println("\nУдалим некоторых животных из вольеров:");
        carnivorousCage.delete(lion);
        herbivoreCage.delete(duck);

        System.out.println("\nПолучим ссылки на некоторых животных:");
        Animal lion2 = carnivorousCage.get("Лев");//льва нет в вольере, мы его удалили
        Animal beaver2 = herbivoreCage.get("Бобер");
        Animal elephant2 = carnivorousCage.get("Слон");//слон в другом вольере
        Animal elephant3 = herbivoreCage.get("Слон");
        Animal eagle3 = carnivorousCage.get("Орел");
    }
}
