package food;

public class Food {
    protected int caloricity;
    protected String type;

    public int getCaloricity() {
        return caloricity;
    }

    public void setCaloricity(int caloricity) {
        this.caloricity = caloricity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Food object = (Food) obj;
        return (this.caloricity == object.getCaloricity() && (this.type != null && this.type.equals(object.getType())));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.caloricity;
        result = prime * result + this.type.hashCode();
        return result;
    }
}
