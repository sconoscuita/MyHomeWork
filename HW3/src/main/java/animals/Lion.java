package animals;

public class Lion extends Carnivorous implements Run, Voice {

    public Lion(int energy, String name) {
        this.energy = energy;
        this.name = name;
        this.area = 10;
    }

    @Override
    public void run() {
        if (super.energy >= Run.kcal) {
            System.out.println("Лев побежал!");
            super.energy -= Run.kcal;
        } else {
            System.out.println("Лев не может бежать! Срочно накормите льва.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.kcal) {
            super.energy -= Voice.kcal;
            return "Лев зарычал";
        } else {
            return "Лев совсем озверел, срочно накормите льва!";
        }
    }
}
