package animals;

public interface Run {
    static final int kcal = 20;

    void run();
}
