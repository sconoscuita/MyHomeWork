package animals;

public class Fish extends Carnivorous implements Swim {

    public Fish(int energy, String name) {
        this.energy = energy;
        this.name = name;
        this.area = 1;
    }

    @Override
    public void swim() {
        if (super.energy >= Swim.kcal) {
            System.out.println("Проплыла рыба");
            super.energy -= Swim.kcal;
        } else {
            System.out.println("Рыба не может плыть! Срочно накормите рыбу.");
        }
    }
}
