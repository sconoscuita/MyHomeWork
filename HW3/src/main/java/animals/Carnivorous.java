package animals;

import food.Food;
import food.Meat;

public class Carnivorous extends Animal {
    public boolean eat(Food food) throws Exception {
            if (food instanceof Meat) {
                return true;
            } else {
                throw new WrongFoodExeption();
            }
        }
    }
