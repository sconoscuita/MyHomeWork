package animals;

public class Duck extends Herbivore implements Fly, Run, Swim, Voice {

    public Duck(int energy, String name)   {
        this.energy = energy;
        this.name = name;
        this.area = 1;
    }

    @Override
    public void fly() {
        if (this.energy >= Fly.kcal) {
            System.out.println("Утка полетела!");
            super.energy -= Fly.kcal;
        } else {
            System.out.println("Утка не может взлететь! Срочно накормите утку.");
        }
    }

    @Override
    public void run() {
        if (super.energy >= Run.kcal) {
            System.out.println("Утка побежала!");
            super.energy -= Run.kcal;
        } else {
            System.out.println("Утка не может бежать! Срочно накормите утку.");
        }
    }

    @Override
    public void swim() {
        if (super.energy >= Swim.kcal) {
            System.out.println("Проплыла утка");
            super.energy -= Swim.kcal;
        } else {
            System.out.println("Утка не может плыть! Срочно накормите утку.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.kcal) {
            super.energy -= Voice.kcal;
            return "Утка прокрякала";
        } else {
            return "Утка теряет сознание, срочно накормите утку!";
        }
    }
}
