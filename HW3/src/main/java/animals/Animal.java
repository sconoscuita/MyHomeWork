package animals;

import food.Food;

public abstract class Animal {
    protected int area;
    protected Food food;
    protected String name;
    protected int energy;

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArea() {
        return area;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Animal object = (Animal) obj;
        return (this.area == object.getArea()
                && (this.name == object.getName() || this.name != null && name.equals(object.getName())));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.area;
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }

    public abstract boolean eat(Food food) throws Exception;
}
