package animals;

public interface Fly {
    static final int kcal = 50;

    void fly();
}
