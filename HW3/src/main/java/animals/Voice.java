package animals;

public interface Voice {
    static final int kcal = 20;

    String voice();
}
