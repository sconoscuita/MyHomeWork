package animals;

public class Beaver extends Herbivore implements Run, Swim {

    public Beaver(int energy, String name)  {
        this.energy = energy;
        this.name = name;
        this.area = 5;
    }

    @Override
    public void run() {
        if (this.energy >= Run.kcal) {
            System.out.println("Бобер пробежал!");
            this.energy -= Run.kcal;
        } else {
            System.out.println("Бобер не может бежать! Срочно накормите бобра");
        }
    }

    @Override
    public void swim() {
        if (super.energy >= Swim.kcal) {
            System.out.println("Проплыл бобер");
            super.energy -= Swim.kcal;
        } else {
            System.out.println("Бобер не может плыть! Срочно накормите бобра");
        }
    }
}
