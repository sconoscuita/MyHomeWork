package animals;

public interface Swim {
    static final int kcal = 30;

    void swim();
}
