package animals;

public class Eagle extends Carnivorous implements Fly, Voice {

    public Eagle(int energy, String name) {
        this.energy = energy;
        this.name = name;
        this.area = 5;
    }

    @Override
    public void fly() {
        if (super.energy >= Fly.kcal) {
            System.out.println("Орел полетел!");
            super.energy -= Fly.kcal;
        } else {
            System.out.println("Орел не может взлететь! Срочно накормите орла.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.kcal) {
            super.energy -= Voice.kcal;
            return "Орлиный крик";
        } else {
            return "Орел теряет сознание, срочно накормите орла!";
        }
    }
}
