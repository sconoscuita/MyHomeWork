package animals;

import food.Food;
import food.Grass;

public class Herbivore extends Animal {
    public boolean eat(Food food) throws Exception {
        if (food instanceof Grass) {
            return true;
        } else {
            throw new WrongFoodExeption();
        }
    }
}
