package animals;

public class Elephant extends Herbivore implements Run, Voice {

    public Elephant(int energy, String name) {
        this.energy = energy;
        this.name = name;
        this.area = 20;
    }

    @Override
    public void run() {
        if (super.energy >= Run.kcal) {
            System.out.println("Слон побежал!");
            super.energy -= Run.kcal;
        } else {
            System.out.println("Слон не может бежать! Срочно накормите слона.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.kcal) {
            super.energy -= Voice.kcal;
            return "Слон подает голос";
        } else {
            return "Слон теряет сознание, срочно накормите слона!";
        }
    }
}
