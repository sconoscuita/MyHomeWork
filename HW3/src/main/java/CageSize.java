public enum CageSize {
    SMALL("Маленький", 5), //5 м2
    MEDIUM("Средний", 10), //10 м2
    LARGE("Большой", 20), //20 м2
    EXTRA_LARGE("Очень большой", 50); //50 м2

    private String size;
    private int sqMeter;

    CageSize(String size, int sqMeter) {
        this.size = size;
        this.sqMeter = sqMeter;
    }

    public int getSqMeter() {
        return sqMeter;
    }

    public void setSqMeter(int sqMeter) {
        this.sqMeter = sqMeter;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
