import org.mockito.InOrder;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.times;

public class CalcTest {
    private Console console;
    private Calculator calculator;

    @BeforeTest
    void setUp() {
        console = Mockito.mock(Console.class);
        calculator = new Calculator(console);
    }

    @DataProvider
    public Object[][] positiveProvider() {
        return new Object[][]{
                //произведение
                {"*", 6, 2, 3},
                {"*", 1, -0.5, -2},
                {"*", 0, 6, 0},
                {"*", -6, 6, -1},
                //сумма
                {"+", 5, 2, 3},
                {"+", -1, -2.5, 1.5},
                {"+", -5, -2, -3},
                {"+", 6, 6, 0},
                //разность
                {"-", -1, 2, 3},
                {"-", -5, -0.5, 4.5},
                {"-", 1, -2, -3},
                {"-", 6, 6, 0},
                //деление
                {"/", -0.1, 0.3, -3},
                {"/", 0, 0, -3},
                {"/", 6, -6, -1},
                {"/", 0.5, 2.5, 5},
        };
    }

    @DataProvider
    public Object[][] negativeProvider() {
        return new Object[][]{
                //произведение
                {"*", -8, 2, 3},
                {"*", 2, -0.5, -2},
                {"*", 4, 6, 0},
                {"*", 526, 6, -1},
                //сумма
                {"+", 425, 2, 3},
                {"+", 6, -2.5, 1.5},
                {"+", 12, -2, -3},
                {"+", -5, 6, 0},
                //разность
                {"-", 0, 2, 3},
                {"-", 55, -0.5, 4.5},
                {"-", 0, -2, -3},
                {"-", 12, 6, 0},
                //деление
                {"/", 0.1, 0.3, -3},
                {"/", 14, 0, -3},
                {"/", -8, -6, -1},
                {"/", 0.001, 2.5, 5},
        };
    }

    @DataProvider
    public Object[][] limitValueProvider() {
        return new Object[][]{
                {"/", "Ошибка: нельзя делить на ноль", "2", "0"},
                {"*", "Ошибка: введенный символ не является числом", "g", "-2"},
                {"+", "Ошибка: введенный символ не является числом", "2", " "},
                {"kfngf", "Ошибка: операция выбрана некорректно", "2", "2"},

        };
    }

    @Test(dataProvider = "positiveProvider")
    public void positiveTest(String operation, double x, double y, double z) {
        Mockito.when(console.readString()).thenReturn(Double.toString(y)).thenReturn(operation).thenReturn(Double.toString(z));
        Assert.assertTrue((Math.abs(Double.parseDouble(calculator.operation()) - x)) < 0.0001);
    }

    @Test(dataProvider = "negativeProvider")
    public void negativeTest(String operation, double x, double y, double z) {
        Mockito.when(console.readString()).thenReturn(Double.toString(y)).thenReturn(operation).thenReturn(Double.toString(z));
        Assert.assertFalse((Math.abs(Double.parseDouble(calculator.operation()) - x)) < 0.0001);
    }

    @Test(dataProvider = "limitValueProvider")
    public void limitTest(String operation, String x, String y, String z) {
        Mockito.when(console.readString()).thenReturn(y).thenReturn(operation).thenReturn(z);
        Assert.assertEquals(calculator.operation(), x);
    }
}