
public class Main {
    public static void main(String[] args) {
        Console console = new Console();
        Calculator calculator = new Calculator(console);

        while (true) {
            console.out(calculator.operation());
        }
    }
}