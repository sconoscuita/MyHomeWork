public class Calculator {
    private final Console console;

    public Calculator(Console console) {
        this.console = console;
    }

    public String operation() {
        String result;
        String operation;
        double x;
        double y;

        console.out("Введите число №1:");
        try {
            x = Double.parseDouble(console.readString());
        } catch (NumberFormatException notDigit) {
            return ("Ошибка: введенный символ не является числом");
        }

        console.out("Введите действие, которое планируете выполнить\n" +
                "(\"+\" - сложение, \"-\" - вычитание, \"*\" - умножение, \"/\" - деление):");
        operation = console.readString();
        if (!((operation.equals("+") || operation.equals("-") || operation.equals("/") || operation.equals("*")))) {
            return ("Ошибка: операция выбрана некорректно");
        }

        console.out("Введите число №2:");
        try {
            y = Double.parseDouble(console.readString());
            if (y == 0 && operation.equals("/")) {
                return ("Ошибка: нельзя делить на ноль");
            }
        } catch (NumberFormatException notDigit) {
            return ("Ошибка: введенный символ не является числом");
        }

        switch (operation) {
            case "+":
                result = Double.toString(x + y);
                break;
            case "-":
                result = Double.toString(x - y);
                break;
            case "*":
                result = Double.toString(x * y);
                break;
            case "/":
                result = Double.toString(x / y);
                break;
            default:
                result = "Указанная математическая операция не поддерживается калькулятором";
        }
        return result;
    }
}