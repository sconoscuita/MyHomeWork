import java.io.PrintStream;
import java.util.Scanner;

public class Console {
    private PrintStream out;
    private Scanner sc;

    public Console() {
        this.out = System.out;
        this.sc = new Scanner(System.in);
    }

    public void out(String message) {
        out.println(message);
    }

    public String readString() {
        return sc.nextLine();
    }
}