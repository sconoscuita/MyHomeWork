import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Random;
import static io.restassured.RestAssured.given;


public class HomeTaskApiTest {
    private Order order;
    private int petId;
    private int id;
    private int quantity;

    @BeforeClass

    public void prepare() throws IOException {

        System.getProperties().load(ClassLoader.getSystemResourceAsStream("my.properties"));

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://213.239.217.15:9090/api/v3/store")
                .addHeader("api_key", System.getProperty("api_key"))
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }

    @BeforeMethod
    void setUp() {
        order = new Order();
        petId = Integer.parseInt(System.getProperty("petId"));
        id = new Random().nextInt(999999);
        quantity = new Random().nextInt(10);
        order.setPetId(petId);
        order.setId(id);
        order.setQuantity(quantity);
    }

    @Test()
    public void checkObjectSave() {

        given()
                .body(order)
                .when()
                .post("/order")
                .then()
                .statusCode(200);

        Order actual =
                given()
                        .pathParam("orderId", order.getId())
                        .when()
                        .get("/order/{orderId}")
                        .then()
                        .statusCode(200)
                        .extract().body()
                        .as(Order.class);
        Assert.assertEquals(actual.getId(), order.getId());
    }

    @Test
    public void checkObjectDelete() {

        given()
                .pathParam("orderId", order.getId())
                .when()
                .get("/order/{orderId}")
                .then()
                .statusCode(404);

        given()
                .body(order)
                .when()
                .post("/order")
                .then()
                .statusCode(200);

        given()
                .pathParam("orderId", order.getId())
                .when()
                .delete("/order/{orderId}")
                .then()
                .statusCode(200);

        given()
                .pathParam("orderId", order.getId())
                .when()
                .get("/order/{orderId}")
                .then()
                .statusCode(404);
    }

    @Test
    public void checkGetInventory() {
        given()
                .when()
                .get("/inventory")
                .then()
                .statusCode(500);
    }
}
