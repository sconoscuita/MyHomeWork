package animals;

public class Elephant extends Herbivore implements Run, Voice {

    public Elephant(int energy, String name) {
        super(energy, name);
    }

    @Override
    public void run() {
        if (super.energy >= Run.energy) {
            System.out.println("Слон побежал!");
            super.energy -= Run.energy;
        }
        else {
            System.out.println("Слон не может бежать! Срочно накормите слона.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.energy) {
            super.energy -= Voice.energy;
            return "Слон подает голос";
        }
        else {
            return "Слон теряет сознание, срочно накормите слона!";
        }
    }
}
