package animals;

public interface Fly {
    static final int energy = 50;
    void fly();
}
