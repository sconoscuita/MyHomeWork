package animals;

import food.Food;
import food.Grass;

public class Carnivorous extends Animal {
    public Carnivorous(int energy, String name) {
        super(energy, name);
    }
    public boolean eat(Food food) {
        if (food instanceof Grass) {
            System.out.print("Хищники не едят траву! ");
            return false;
        }
        else {
            return true;
        }
    }
}
