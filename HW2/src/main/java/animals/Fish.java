package animals;

public class Fish extends Carnivorous implements Swim {

    public Fish(int energy, String name) {
        super(energy, name);
    }

    @Override
    public void swim() {
        if (super.energy >= Swim.energy) {
            System.out.println("Проплыла рыба");
            super.energy -= Swim.energy;
        }
        else{
            System.out.println("Рыба не может плыть! Срочно накормите рыбу.");
        }
    }
}
