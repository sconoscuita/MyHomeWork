package animals;

public class Eagle extends Carnivorous implements Fly, Voice {
    public Eagle(int energy, String name) {
        super(energy, name);
    }

    @Override
    public void fly() {
        if (super.energy >= Fly.energy) {
            System.out.println("Орел полетел!");
            super.energy -= Fly.energy;
        }
        else {
            System.out.println("Орел не может взлететь! Срочно накормите орла.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.energy) {
            super.energy -= Voice.energy;
            return "Орлиный крик";
        }
        else {
            return "Орел теряет сознание, срочно накормите орла!";
        }
    }
}
