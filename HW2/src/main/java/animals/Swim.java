package animals;

public interface Swim {
    static final int energy = 30;
    void swim();
}
