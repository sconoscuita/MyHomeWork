package animals;

import food.Food;

public abstract class Animal {
    protected Food food;
    protected int energy;
    protected String name;

    public Animal(int energy, String name) {
        this.energy = energy;
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract boolean eat(Food food);
}
