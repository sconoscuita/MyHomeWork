package animals;

public class Duck extends Herbivore implements Fly, Run, Swim, Voice {
    public Duck(int energy, String name) {
        super(energy, name);
    }
    @Override
    public void fly() {
        if (super.energy >= Fly.energy) {
            System.out.println("Утка полетела!");
            super.energy -= Fly.energy;
        }
        else {
            System.out.println("Утка не может взлететь! Срочно накормите утку.");
        }
    }

    @Override
    public void run() {
        if (super.energy >= Run.energy) {
            System.out.println("Утка побежала!");
            super.energy -= Run.energy;
        }
        else {
            System.out.println("Утка не может бежать! Срочно накормите утку.");
        }
    }

    @Override
    public void swim() {
        if (super.energy >= Swim.energy) {
            System.out.println("Проплыла утка");
            super.energy -= Swim.energy;
        }
        else{
            System.out.println("Утка не может плыть! Срочно накормите утку.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.energy) {
            super.energy -= Voice.energy;
            return "Утка прокрякала";
        }
        else {
            return "Утка теряет сознание, срочно накормите утку!";
        }
    }
}
