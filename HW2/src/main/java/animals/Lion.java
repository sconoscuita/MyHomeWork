package animals;

public class Lion extends Carnivorous implements Run, Voice {
    public Lion(int energy, String name) {
        super(energy, name);
    }

    @Override
    public void run() {
        if (super.energy >= Run.energy) {
            System.out.println("Лев побежал!");
            super.energy -= Run.energy;
        }
        else {
            System.out.println("Лев не может бежать! Срочно накормите льва.");
        }
    }

    @Override
    public String voice() {
        if (super.energy >= Voice.energy) {
            super.energy -= Voice.energy;
            return "Лев зарычал";
        }
        else {
            return "Лев совсем озверел, срочно накормите льва!";
        }
    }
}
