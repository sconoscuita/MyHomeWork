package animals;

public class Beaver extends Herbivore implements Run, Swim {

    public Beaver(int energy, String name) {
        super(energy, name);
    }

    @Override
    public void run() {
        if (super.energy >= Run.energy) {
            System.out.println("Бобер пробежал!");
            super.energy -= Run.energy;
        } else {
            System.out.println("Бобер не может бежать! Срочно накормите бобра");
        }
    }

    @Override
    public void swim() {
        if (super.energy >= Swim.energy) {
            System.out.println("Проплыл бобер");
            super.energy -= Swim.energy;
        } else {
            System.out.println("Бобер не может плыть! Срочно накормите бобра");
        }
    }
}
