package animals;

import food.Food;
import food.Meat;

public class Herbivore extends Animal {
    public Herbivore(int energy, String name) {
        super(energy, name);
    }

    public boolean eat(Food food) {
        if (food instanceof Meat) {
            System.out.print("Травоядные не едят мясо! ");
            return false;
        }
        else {
            return true;
        }
    }
}
