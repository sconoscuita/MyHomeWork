package animals;

public interface Run {
    static final int energy = 20;
    void run();
}
