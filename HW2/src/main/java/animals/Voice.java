package animals;

public interface Voice {
    static final int energy = 20;
    String voice();
}
