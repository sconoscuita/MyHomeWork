import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker();

        Grass grass = new Grass(100);
        Meat meat = new Meat(200);

        Beaver beaver = new Beaver(50, "Бобер");
        Duck duck = new Duck(50, "Утка");
        Elephant elephant = new Elephant(50, "Слон");

        Eagle eagle = new Eagle(50, "Орел");
        Fish fish = new Fish(50, "Рыба");
        Lion lion = new Lion(50, "Лев");

        System.out.println("\n" + "Посмотрим на пруд:" + "\n");
        Swim[] lake = {beaver, duck, fish};
        for (int i = 0; i < lake.length; i++) {
            lake[i].swim();
        }

        System.out.println("\n" + "Попросим животных подать голос:" + "\n");
        worker.getVoice(duck);
        worker.getVoice(eagle);
        worker.getVoice(lion);

        System.out.println("\n" + "Покормим животных:" + "\n");
        worker.feed(lion, grass);
        worker.feed(lion, meat);
        worker.feed(beaver, meat);
        worker.feed(beaver, grass);
        worker.feed(elephant, grass);
        worker.feed(eagle, meat);

        System.out.println("\n" + "Поиграем с уткой:" + "\n");
        duck.fly();
        duck.run();
        duck.swim();
        worker.getVoice(duck);
        worker.feed(duck, grass);
    }
}
