package food;

import animals.Animal;

public abstract class Food {
    private int caloricity;

    public int getCaloricity() {
        return caloricity;
    }

    public void setCaloricity(int caloricity) {
        this.caloricity = caloricity;
    }

    public Food(int caloricity) {
        this.caloricity = caloricity;
    }
}
