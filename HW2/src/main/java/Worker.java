import animals.Animal;
import animals.Voice;
import food.Food;

public class Worker {
    public void feed(Animal animal, Food food) {
        boolean x = animal.eat(food);
        if (x) {
            animal.setEnergy(animal.getEnergy() + food.getCaloricity());
            System.out.println(animal.getName() + " получил(а) порцию еды. В запасе " + animal.getEnergy() + " ккал свободной энергии.");
        }
        else {
            System.out.println(animal.getName() + " сегодня без ужина.");
        }
    }
    public void getVoice(Voice animalVoice) {
        System.out.println(animalVoice.voice());
    }
}
